#include <OneWire.h>
#include <i2cmaster.h>  
#include <inttypes.h>
 
 
#define CRC8INIT  0x00
#define CRC8POLY  0x18              //0X18 = X^8+X^5+X^4+X^0

  
      int dev = 0x5A<<1; //this is used to read temprature from register 
      int devE = 0x00; // I use the general address. If I specify the address
                //(0x05<<1), the code doesn't work, don't know why... yet.
      int data_low = 0;
      int data_high = 0;
      int pec = 0;
      int countif= 0;
      float emissivity = 0;
      float newemissivity;
      char junk = ' ';
  
  void setup(){
  	Serial.begin(9600);
  	Serial.println("Setup...");
  	
  	i2c_init(); //Initialise the i2c bus
  	//PORTC = (1 << PORTC4) | (1 << PORTC5);//enable pullups
     
      delay(5000);delay(1000);

              Serial.println("Enter value for emissivity, Press ENTER");
              while (Serial.available() == 0) ;  // Wait here until input buffer has a character
              {
                  //Side 1
                  newemissivity = Serial.parseFloat();        // new command in 1.0 forward
                  Serial.print("New emissivity= "); 
                  Serial.println(newemissivity, DEC);
        
                  while (Serial.available() > 0)  // .parseFloat() can leave non-numeric characters
                  { 
                      junk = Serial.read() ; 
                  }  // clear the keyboard buffer
              }
      
     
              unsigned int data_l = 0; 
              unsigned int data_h = 0; 
              int pec = 0; 
              float data_t = 0;
              
              byte emissivity_low;
              byte emissivity_high;
              byte sending_pec;
             
              
              
              //READ EEPROM/RAM
              Serial.println("*1: Read EEPROM address:");
              i2c_start_wait(devE+I2C_WRITE);
              i2c_write(0x24);  //0x004 and 0x04 etc reads the same address in the RAM,
                                //add 2(0) for EEPROM, eg. 0x24 (emissivity correction
                                //coefficient in EEPROM).
              
              i2c_rep_start(devE+I2C_READ);
              data_l = i2c_readAck(); //Read 1 byte and then send ack
              data_h = i2c_readAck(); //Read 1 byte and then send ack
              pec = i2c_readNak();
              i2c_stop();
              
              Serial.print("Data Low: ");
              Serial.println(data_l);
              
              Serial.print("Data High: ");
              Serial.println(data_h);
              
              Serial.print("Data combined: ");
              data_t = (((data_h) << 8) + data_l);
              Serial.println(data_t);
              
              Serial.print("Emissivity: ");
              emissivity = ((data_t) / 65535);
              Serial.println(emissivity);
              
              delay(3000);

              
              
              //We want to WRITE TO EEPROM, 
              //FIRST: ERASE OLD STUFF
              Serial.println("*2: Erasing old emissivity factor (writing 0).");
              i2c_start_wait(devE+I2C_WRITE); 
              i2c_write(0x24); //Register Address to write to
              
              i2c_write(0x00); //Erase low byte (write 0)
              i2c_write(0x00); //Erase high byte (write 0)
              i2c_write(0xE8); //Send PEC
              i2c_stop();
              delay(3000);
              
              //CHECK IF THE EEPROM VALUE HAS BEEN ERASED
              Serial.println("*3: Check if the old emissivity coefficient was erased:");
              i2c_start_wait(devE+I2C_WRITE); 
              i2c_write(0x24);  //See above comment.
              
              i2c_rep_start(devE+I2C_READ); 
              data_l = i2c_readAck(); //Read 1 byte and then send ack 
              data_h = i2c_readAck(); //Read 1 byte and then send ack 
              pec = i2c_readNak(); 
              i2c_stop(); 
              
              Serial.print("Data Low: ");
              Serial.println(data_l);
              
              Serial.print("Data High: ");
              Serial.println(data_h);
              
              Serial.print("Data combined: ");
              data_t = (((data_h) << 8) + data_l); 
              Serial.println(data_t);
              
              Serial.print("Emissivity: ");
              emissivity = ((data_t) / 65535);
              Serial.println(emissivity);
              delay(2000);
              
              //WRITE TO EEPROM, THE NEW STUFF!
              Serial.println("*4: Write newemissivity to EEPROM .");              
             
              float tempemissivity = 65535 * newemissivity;
              unsigned int emissivityINT = (int) tempemissivity;
              Serial.print("Value of emissivityINT (entered emissivity * 65535): ");
              Serial.println(  emissivityINT,DEC);
              
              //Seperating the emissivoty into two bytes 
              byte input [3] ; // having an aray which first elememnt is address and its second is low byte data and third is 
                               // high byte data. 
                               
                               
                                           
              input [0] = 0x24;
              input [1] = (byte) emissivityINT ;// input [1] contains the low byte of the tempemissivity
              emissivityINT  = ((emissivityINT ) >> 8);
              input [2]= (byte) emissivityINT ; // input [2] contains the high byte of the tempemissivity

              sending_pec =OneWire::crc8( input, 3 );
              // OneWire library is provided in this link: http://www.pjrc.com/teensy/td_libs_OneWire.html
              
              //sending_pec =  crc8 ( input, 3 ); 
              // the function crc8 is suggested by Dr, Jesus from link https://code.google.com/p/avr-code/source/browse/trunk/thermo/crc8.c?r=5

              // None of them work ????
              //Value of the sending_pec does not match the value given by CRC-8 calculator provided in http://smbus.org/faq/crc8Applet.htm



              Serial.print("Address stored in input [0]: ");
              Serial.println(  input [0],BIN);
              
              Serial.print("Low byte of data in input [1]: ");
              Serial.println(  input [1],BIN);
              
              Serial.print("High byte of data in input [2]: ");
              Serial.println(  input [2],BIN);
              
              Serial.print("Value of sending_pec : ");
              Serial.println(  sending_pec,BIN); 
              
              
              i2c_start_wait(devE+I2C_WRITE);
              i2c_write(input[0]); //Register Address to write to
              i2c_write(input[1]); // sending the low byte 
              i2c_write(input[2]);// sending the high byte 
              i2c_write(sending_pec); //Send PEC
              i2c_stop();
             
             
              /*
              //These are the values for emissivity of 0.68 which are calculated by hand
              // HEX(0.68 * 65535) = 0xAE0D
              // low byte : AE
              // High byte : 0D
              // based on CRC-8 calculater in http://smbus.org/faq/crc8Applet.htm   
              // by typing 240DAE in the above applet we give 0x42 which is the PEC.
              i2c_start_wait(devE+I2C_WRITE);
              i2c_write(0x24); //Register Address to write to
              i2c_write(0x0D); // sending the low byte
              i2c_write(0xAE); // sending the high byte 
              i2c_write(0x42); //Send PEC
              i2c_stop(); */
              
              delay(5000);
              
              
              //CHECK IF THE EEPROM VALUE HAS BEEN updated to new value of emissivity
              Serial.println("*5: Check if the new emissivity coefficient has written or not:");
              i2c_start_wait(devE+I2C_WRITE); 
              i2c_write(0x24);  //See above comment.
              
              i2c_rep_start(devE+I2C_READ); 
              data_l = i2c_readAck(); //Read 1 byte and then send ack 
              data_h = i2c_readAck(); //Read 1 byte and then send ack 
              pec = i2c_readNak(); 
              i2c_stop(); 
              
              Serial.print("Data Low: ");
              Serial.println(data_l);
              
              Serial.print("Data High: ");
              Serial.println(data_h);
              
              Serial.print("Data combined: ");
              data_t = (((data_h) << 8) + data_l); 
              Serial.println(data_t);
              
              Serial.print("Emissivity: ");
              emissivity = ((data_t) / 65535);
              Serial.println(emissivity);
              delay(3000);
              
              
              
              Serial.println("----------Reading temprature starts now----------");
              delay(5000);
  
  }
  

  void loop(){


      /*************************************************************************
       void i2c_start_wait(unsigned char address)
       Issues a start condition and sends address and transfer direction.
       If device is busy, use ack polling to wait until device is ready
       
       Input:   address and transfer direction of I2C device
      *************************************************************************/
      i2c_start_wait(dev+I2C_WRITE); //You can interpret Write as send. 

      /*************************************************************************
        unsigned char i2c_write( unsigned char data )
        Send one byte to I2C device
        
        Input:    byte to be transfered
        Return:   0 write successful 
                  1 write failed
      *************************************************************************/
      i2c_write(0x07); // sending a byte (contais both command and address) 
      /************************************************************************
      Note:
      Opcode Command
      000x xxxx* RAM Access
      001x xxxx* EEPROM Access
      1111_0000** Read Flags
      in out case i2c_write(0x07)->we are writing to EEPROM in other owrds it's a EEPROM access.
      Be carefull we are not writing to EEPROM we are actually sending a byte that patially is command and patially address. 
      *************************************************************************/
      
      
      
      // read
      // #define I2C_READ    1: defines the data direction (reading from I2C device) in i2c_start(),i2c_rep_start() 
      /*************************************************************************
       unsigned char i2c_rep_start(unsigned char address) 
       Issues a repeated start condition and sends address and transfer direction 
      
       Input:   address and transfer direction of I2C device
       
       Return:  0 device accessible
                1 failed to access device
      *************************************************************************/
      i2c_rep_start(dev+I2C_READ);
      /*************************************************************************
       unsigned char i2c_readAck(void)
       Read one byte from the I2C device, request more data from device 
       
       Return:  byte read from I2C device
      *************************************************************************/
      data_low = i2c_readAck(); //Read 1 byte and then send ack
      data_high = i2c_readAck(); //Read 1 byte and then send ack
      /*************************************************************************
      unsigned char i2c_readNak(void)
       Read one byte from the I2C device, read is followed by a stop condition 
       
       Return:  byte read from I2C device
      *************************************************************************/
      pec = i2c_readNak();
      /*************************************************************************
       void i2c_stop(void)
       Terminates the data transfer and releases the I2C bus
      *************************************************************************/
      i2c_stop();
      
      //This converts high and low bytes together and processes temperature, MSB is a error bit and is ignored for temps
      double tempFactor = 0.02; // 0.02 degrees per LSB (measurement resolution of the MLX90614)
      double tempData = 0x0000; // zero out the data
      int frac; // data past the decimal point
      
      // This masks off the error bit of the high byte, then moves it left 8 bits and adds the low byte.
      tempData = (double)(((data_high & 0x007F) << 8) + data_low);
      tempData = (tempData * tempFactor)-0.01;
      
      float celcius = tempData - 273.15;
      float fahrenheit = (celcius*1.8) + 32;
  
      Serial.print("Celcius: ");
      Serial.println(celcius);
  
      Serial.print("Fahrenheit: ");
      Serial.println(fahrenheit);
      
      countif++; // adding to count so that it goes to the above if condition only once.
      
      delay(1000); // wait a second before printing again
      
  }
  
  uint8_t crc8 ( uint8_t *data_in, uint16_t number_of_bytes_to_read )
{
  uint8_t  crc;
  uint16_t loop_count;
  uint8_t  bit_counter;
  uint8_t  data;
  uint8_t  feedback_bit;
  
  crc = CRC8INIT;

  for (loop_count = 0; loop_count != number_of_bytes_to_read; loop_count++)
  {
    data = data_in[loop_count];
    
    bit_counter = 8;
    do {
      feedback_bit = (crc ^ data) & 0x01;
  
      if ( feedback_bit == 0x01 ) {
        crc = crc ^ CRC8POLY;
      }
      crc = (crc >> 1) & 0x7F;
      if ( feedback_bit == 0x01 ) {
        crc = crc | 0x80;
      }
    
      data = data >> 1;
      bit_counter--;
    
    } while (bit_counter > 0);
  }
  
  return crc;
}

char *MakeCRC(char *BitString)
   {
   static char Res[9];                                 // CRC Result
   char CRC[8];
   int  i;
   char DoInvert;
   
   for (i=0; i<8; ++i)  CRC[i] = 0;                    // Init before calculation
   
   for (i=0; i<strlen(BitString); ++i)
      {
      DoInvert = ('1'==BitString[i]) ^ CRC[7];         // XOR required?

      CRC[7] = CRC[6];
      CRC[6] = CRC[5];
      CRC[5] = CRC[4] ^ DoInvert;
      CRC[4] = CRC[3] ^ DoInvert;
      CRC[3] = CRC[2];
      CRC[2] = CRC[1];
      CRC[1] = CRC[0];
      CRC[0] = DoInvert;
      }
      
   for (i=0; i<8; ++i)  Res[7-i] = CRC[i] ? '1' : '0'; // Convert binary to ASCII
   Res[8] = 0;                                         // Set string terminator

   return(Res);
   }


